package com.wjs.examfrog.other.controller;

import com.wjs.examfrog.common.CommonResult;
import com.wjs.examfrog.common.Result;
import com.wjs.examfrog.other.service.AreaService;
import com.wjs.examfrog.vo.AreaVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "后台管理——分区")
@RestController
@RequestMapping("/area")
public class AreaController {

    @Resource
    private AreaService areaService;

    @Value("${server.port}")
    private String port;

    @ApiOperation(value = "后台管理 获取全部分区")
    @GetMapping("/listAll")
    public ResponseEntity<Result<List<AreaVO>>> listAllForManage(){
        List<AreaVO> areaVOS = areaService.listAllForManage();
        return CommonResult.success(areaVOS);
    }

    @GetMapping("/test")
    public ResponseEntity<Result<String>> testBalance(@RequestParam("key") String key){
        return CommonResult.success("负载均衡，端口号为：" + port);
    }
}
