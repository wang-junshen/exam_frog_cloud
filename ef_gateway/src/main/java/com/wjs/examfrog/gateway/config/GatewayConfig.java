package com.wjs.examfrog.gateway.config;

import com.wjs.examfrog.gateway.filter.CustomLoadBalancerFilter;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.gateway.config.LoadBalancerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class GatewayConfig {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public CustomLoadBalancerFilter customLoadBalancerFilter(LoadBalancerClient client,
                                                             LoadBalancerProperties properties,
                                                             DiscoveryClient discoveryClient){
        return new CustomLoadBalancerFilter(client,properties,discoveryClient);
    }
}
